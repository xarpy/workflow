# WorkFlow ResAPI
> projeto de uma API Restful desenvolvida em flask

[![Python Version][python-image]][python-url]
[![Flask Version][flask-image]][flask-url]

## Introdução

Pequena API, criado em Python e framework Flask, baseado num principio de um gestor de fluxo de trabalhos, na qual trabalha cadastrando novos fluxos, atualizando antigos e exportando em CSV lista de atividades da ultima cadastrada.

### Prerequisitos

Segue a lista de pacotes utilizados no projeto.

Package                                      | Version  |
---------------------------------------------| ---------|
[Flask][flask-url]                           | 1.1.1    |
[Flask-Cors][flask_cors-url]                 | 3.0.8    |
[Flask-Migrate][flask_migrate-url]           | 2.5.2    |
[flask-Restful][flask_restful-url]           | 0.3.7    |
[Flask-SQLAlchemy][flask_sqllchemy-url]      | 2.4.4    |
[gunicorn][gunicorn-url]                     | 20.0.4   |
[python-dotenv][python_dotenv-url]           | 0.10.3   |
[psycopg2][psycopg2-url]                     | 2.8.4    |



## Exemplo de uso:

O produto criado foi feito para fins de testes pessoais, utilizando alguns padrões especificos. O intuito nada mais é que fazer uso de novas tecnologias e pacotes, além de aprender novas tendências e tecnologias. Para fim de testes, fizemos uso do software Postman, por conta da praticidade e eficência. Entretanto para aqueles que desejam realizar testes manuais, segue instruções para uso com cURL.

**ENDPOINTS E FUNÇÃO**

Rotas                                       | Metodos    |
--------------------------------------------|------------|
http://127.0.0.1:5000/workflow              | GET e POST |
http://127.0.0.1:5000/workflow/consume      | GET        |
http://127.0.0.1:5000/workflow/<string:uuid>| PATCH      |

1. No primeiro endpoint, está habilitado para receber requisições por meio dos verbos GET e POST. Quando se faz um GET neste acesso, ele retornará uma lista geral de todos os workflows cadastrados. Enquanto que no tipo POST ele irá cadastrar os movos workflows.Para tal, deve ser enviado os campos ```data``` e ```steps```, sendo respectivamente uma lista de objetos json de estruturas iguais e uma lista de strings. Segue abaixo exemplo do payload:
```
{
   "data":[
      {
         "key1":"value1",
         "key2":"value2"
      },
      {
         "key1":"value1",
         "key2":"value2"
      }
   ],
   "steps":[
      "First,
      "Second ",
      "Third"]
}
```
2. No segundo endpoint o mesmo irá atualizar a informação referente ao utlimo workflow da lista e retornar uma arquivo .csv dos dados inserido em ```data```.
3. No terceiro endpoint, o mesmo irá apenas atualizar as informações de um determinado wokflow, baseado no uuid do mesmo(*devendo ser repassado por meio da URL)*.


**EXEMPLOS:**

```curl
**Metodo - GET:**

curl -X GET http://127.0.0.1:5000/workflow -H 'Accept: */*' -H 'Connection: keep-alive' -H 'Content-Type: application/json' -H 'Host: 127.0.0.1:5000'

```
```curl
**Metodo - GET:**

curl -X GET http://127.0.0.1:5000/workflow/consume -H 'Accept: */*' -H 'Connection: keep-alive' -H 'Content-Type: application/json' -H 'Host: 127.0.0.1:5000'

```
```curl
**Metodo - POST:**

curl -X POST http://127.0.0.1:5000/workflow -H 'Accept: */*' -H 'Connection: keep-alive' -H 'Content-Type: application/json' -H 'Host: 127.0.0.1:5000' -d '{"data":[{"key1":"value1","key2":"value2"},{"key1":"value1","key2":"value2"}],"steps":["First,"Second","Third"]}'

```
```curl
**Metodo - PATCH:**

curl -X PATCH http://127.0.0.1:5000/workflow/b19f61f5-ac03-4ec7-b062-7ade8edd9fa6 -H 'Accept: */*' -H 'Connection: keep-alive' -H 'Content-Type: application/json' -H 'Host: 127.0.0.1:5000'

```

## Instalação:

Para instalação em ambiente de desenvolvimento, segue o contexto abaixo, explicando. Caso prefira e tenha conhecimentos avançados, utilizamos como servidor e compilador, nginx e gunicorn, logo disponibilizaremos arquivos de configurações caso sabia como implanta-los e queria testa-los em ambiente de homologação.

Devemos em breve, finalizar o projeto com uma versão dockerizada, para fins de teste pratico.

### Etapas para uso em desenvolvimento:
1. Com a sua ambiente python, instale a lista de pacotes.
```sh
pip install -r requeriments/dev.txt
```
2. Não esqueça de criar suas variaveis de ambiente, usem o editor de preferência e insira dentro da pasta do projeto.
Segue exemplo das variaveis exigidas para funcionamento do projeto:

```
[settings]
SECRET_KEY="123456"
DB = "postgresql://user:password@localhost:5432/database"
LOG= "/full/path/to/local/file"

[flask_settings]
FLASK_APP="main.py"
FLASK_ENV="development"
```

3. Na pasta do projeto com as variaveis de ambientes definidas, realize a criação da base de dados do projeto.
```sh
flask db init
```
```sh
flask db migrate
```
```sh
flask db upgrade
```
4. Após a criação da base, apenas agora execute o projeto

```sh
flask run
```

## Meta

Distribuido pela licença GNU GENERAL PUBLIC . Veja ``LICENSE`` para maiores detalhes.


## Contribua

1. Fork it (<https://gitlab.com/xarpy/workflow>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request

<!-- Markdown link & img dfn's -->
[python-image]: https://img.shields.io/badge/python-v3.7-blue
[flask-image]: https://img.shields.io/badge/flask-v1.1.1-blue
[python-url]: https://www.python.org/downloads/release/python-374/
[flask-url]: https://flask.palletsprojects.com/en/1.1.x/
[flask_cors-url]: https://flask-cors.readthedocs.io/en/latest/
[flask_migrate-url]: https://flask-migrate.readthedocs.io/en/latest/
[flask_restful-url]: https://flask-restful.readthedocs.io/en/latest/
[python_dotenv-url]: https://github.com/theskumar/python-dotenv
[requests-url]: https://requests.kennethreitz.org/en/master/
[flask_sqllchemy-url]: https://flask-sqlalchemy.palletsprojects.com/en/2.x/
[gunicorn-url]: http://docs.gunicorn.org/en/latest/index.html
[marshmallow-url]: https://marshmallow.readthedocs.io/en/stable/index.html
[flask_swagger_ui-url]: https://github.com/sveint/flask-swagger-ui
[psycopg2-url]: http://initd.org/psycopg/

