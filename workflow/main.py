from os import getenv

from app import create_app
from app.models import db
from app.utils import get_local
from dotenv import load_dotenv

load_dotenv(dotenv_path=get_local(env=True))

app = create_app(getenv('FLASK_ENV') or 'default')


@app.shell_context_processor
def shell_context():
    return dict(
        app=app,
        db=db,
    )
