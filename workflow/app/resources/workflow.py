from uuid import uuid4

from app.models import Activities, db
from app.utils import create_csv, get_local, send_reponse
from flask import request, send_from_directory
from flask_restful import Resource


class Workflow(Resource):
    def post(self):
        data = request.get_json(force=True, silent=True)
        if data is None:
            return send_reponse({"message": "Missing Json"}, 400)
        try:
            if 'data' not in data:
                return send_reponse(
                    {"message": "You are missing insert data to workflow"},
                    400)
            elif 'steps' not in data:
                return send_reponse(
                    {"message": "You are missing insert steps to workflow"},
                    400)
            work = Activities('inserted', data['data'], data['steps'])
            db.session.add(work)
            db.session.commit()

            msg = dict(
                uuid=work.uuid,
                status=work.status,
                data=work.data,
                steps=work.steps
            )
            return send_reponse(msg, 200)

        except Exception as e:
            db.session.rollback()
            return send_reponse(
                {"message": f"Error is {e}"}, 500)
        finally:
            db.session.close()

    def patch(self, uuid=None):
        if uuid is None:
            return send_reponse({"message": "Missing UIID"}, 400)
        try:
            work = Activities.query.filter_by(uuid=uuid).first()
            work.status = "consumed"
            db.session.add(work)
            db.session.commit()
            return send_reponse({"message": "Workflow updated"}, 200)

        except Exception as e:
            db.session.rollback()
            return send_reponse(
                {"message": f"Error is {e}"}, 500)
        finally:
            db.session.close()

    def get(self):
        try:
            acts = Activities.query.all()
            list_acts = list()
            for act in acts:
                list_acts.append(dict(
                    uuid=act.id,
                    status=act.status,
                    data=act.data,
                    steps=act.steps
                ))
            return send_reponse(list_acts, 200)

        except Exception as e:
            db.session.rollback()
            return send_reponse(
                {"message": f"Error is {e}"}, 500)
        finally:
            db.session.close()


class WorkflowCSV(Resource):
    def get(self):
        PATH = get_local()
        try:
            act = Activities.query.order_by(Activities.id.desc()).first()
            if act.status == "inserted":
                act.status = "consumed"
                db.session.add(act)
                db.session.commit()

            return send_from_directory(PATH, "data.csv", as_attachment=True)

        except Exception as e:
            db.session.rollback()
            return send_reponse(
                {"message": f"Error is {e}"}, 500)
        finally:
            db.session.close()
