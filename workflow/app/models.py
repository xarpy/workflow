from uuid import uuid4

from sqlalchemy.dialects.postgresql import ARRAY, JSONB, UUID

from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


def configure(app):
    db.init_app(app)
    app.db = db


class Activities(db.Model):
    __tablename__ = "workflows"

    id = db.Column(db.Integer, autoincrement=True,
                   primary_key=True, unique=True)
    uuid = db.Column(UUID(as_uuid=True), default=uuid4, unique=True)
    status = db.Column(
        db.Enum('inserted', 'consumed', name='status_types'),
        nullable=False, server_default=("inserted"))
    data = db.Column(JSONB, nullable=False)
    steps = db.Column(ARRAY(db.String), nullable=False)

    def __init__(self, status, data, steps):
        self.status = status
        self.data = data
        self.steps = steps

    def __repr__(self):
        return f"<Activity UUID: {self.uuid}>"
