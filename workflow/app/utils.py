import csv
from os import getcwd
from os.path import dirname, join

from flask import Response, json


def pagination(data, url, start, limit):
    count = len(data)
    if (count < start):
        abort(404)
    return dict(
        start=start,
        limit=limit,
        count=count,
        message=data[(start - 1):(start - 1 + limit)],
    )


def send_reponse(msg, code, mime=None, headers=None):
    default_mime = 'application/json'
    send = Response(
        response=json.dumps(msg),
        status=code,
        mimetype=(default_mime if mime is None else mime)
    )
    if headers is not None:
        for head in headers:
            send.headers.add(head[0], head[1])
    return send


def get_local(env=False):

    if env is True:
        path = join(dirname(__file__), '.env')
        return path
    else:
        path = getcwd()
        return path


def create_csv(data):
    try:
        name_columns = list(data[0].keys())
        with open('data.csv', 'w') as file:
            writer = csv.DictWriter(file, fieldnames=name_columns)
            writer.writeheader()
            writer.writerows(data)
    except IOError as e:
        print(e)
        return e
