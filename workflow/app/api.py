from app.resources.workflow import Workflow, WorkflowCSV
from flask_restful import Api

# Instânciamos a API do FlaskRestful
api = Api()


def configure(app):

    api.add_resource(
        Workflow,
        '/workflow',
        '/workflow/<string:uuid>'
    )
    api.add_resource(WorkflowCSV, '/workflow/consume')
    api.init_app(app)
